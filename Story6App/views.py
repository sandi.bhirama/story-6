from django.shortcuts import render
from django.http import HttpResponse
from .models import Status
from .forms import add_status
from django.contrib import messages
from django.http import HttpResponseRedirect


def addStatus(request):
	if request.method == "POST":
		formStatus = add_status(request.POST)
		if formStatus.is_valid():
			form_item = formStatus.save(commit=True)
			form_item.save()
		return HttpResponseRedirect('/')
	else:
		formStatus = add_status()
		dataStatus = Status.objects.all()
		context = {
			'StatusS': dataStatus,
		}
	return render(request, 'index.html', {'form' : formStatus, 'StatusS': dataStatus})

def deleteStatus(request):
	Status.objects.all().delete()
	return HttpResponseRedirect('/')

