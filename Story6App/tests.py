from django.test import TestCase, Client
from django.urls import resolve
from .views import addStatus
from .models import Status
from .forms import add_status


class Story6est(TestCase):
    def test_Story6_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_Story6_using_index_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_Story6_using_addStatus_func(self):
        found = resolve('/')
        self.assertEqual(found.func,addStatus)
    
    def test_model_can_create_new_status(self):
            new_activity = Status.objects.create(judul_Status='ini judul',isi_Status='Aku mau latihan ngoding deh')
            counting_all_available_activity = Status.objects.all().count()
            self.assertEqual(counting_all_available_activity,1)

    def test_can_save_a_POST_request(self):
        response = self.client.post('/', data={'judul_Status': 'ini judul', 'isi_Status' : 'aku mau latihan ngoding dehh'})
        counting_all_available_activity = Status.objects.all().count()
        self.assertEqual(counting_all_available_activity, 1)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/')
        new_response = self.client.get('/')
        html_response = new_response.content.decode('utf8')
        self.assertIn('aku mau latihan ngoding dehh', html_response)

    def test_Story6_post_success_and_render_the_result(self):
            test = 'Anonymous'
            response_post = Client().post('/', {'judul_Status': test, 'isi_Status': test})
            self.assertEqual(response_post.status_code, 302)
    
            response= Client().get('/')
            html_response = response.content.decode('utf8')
            self.assertIn(test, html_response)

    def test_Story6_post_error_and_render_the_result(self):
            test = 'Anonymous'
            response_post = Client().post('/', {'judul_Status': '', 'isi_Status': ''})
            self.assertEqual(response_post.status_code, 302)
    
            response= Client().get('/')
            html_response = response.content.decode('utf8')
            self.assertNotIn(test, html_response)

    def test_string_representation(self):
        string = Status(judul_Status="ngoding kuy")
        self.assertEqual(str(string), string.judul_Status)

    def test_create_anything_model(self, judul='ini judul', isi='ini isi dtatus'):
            return Status.objects.create(judul_Status = judul, isi_Status=isi)

    

 


