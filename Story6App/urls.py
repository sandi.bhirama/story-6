from django.contrib import admin
from django.urls import path
from django.http import HttpResponse
from django.conf.urls import url
from Story6App import views as story6views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$',story6views.addStatus),
    url(r'^delete/', story6views.deleteStatus)


]